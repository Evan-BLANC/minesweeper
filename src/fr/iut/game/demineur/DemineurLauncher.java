/**
 *
 */
package fr.iut.game.demineur;

import fr.iut.game.demineur.controller.*;
import fr.iut.game.demineur.controller.OptionsStageMenuController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * @author evanblanc, avarnier
 *
 */
public class DemineurLauncher extends Application implements PaneSelectorInterface {


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}


	private BorderPane mainPane;

	private Pane selectPane;
	private Pane gamePane;
	private Pane creditsPane;
	private Pane optionsPane;
	private Pane optionsDifficultyPane;
	private Pane optionsCustomPane;
	private Pane multiPane;
	private Pane attentePane;
	//---------------------
	private FXMLLoader gamePaneLoader;
	private FXMLLoader creditsPaneLoader;
	private FXMLLoader optionsPaneLoader;
	private FXMLLoader optionsDifficultyPaneLoader;
	private FXMLLoader optionsCustomPaneLoader;
	private FXMLLoader selectPaneLoader;
	private FXMLLoader multiPaneLoader;
	private FXMLLoader attentePaneLoader;



	/* (non-Javadoc)
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) throws IOException {

		loadFXMLPanels();

		this.mainPane = new BorderPane();
		Scene scene = new Scene(mainPane);

		primaryStage.centerOnScreen();
		primaryStage.setOnCloseRequest(event -> System.exit(0));
		primaryStage.setHeight(750);
		primaryStage.setWidth(1080);



		primaryStage.setScene(scene);


		primaryStage.show();

		this.mainPane.setCenter(selectPane);
	}

	public void loadFXMLPanels() throws IOException {

		selectPaneLoader = new FXMLLoader(this.getClass().getResource("view/SelectStage.fxml"));
		selectPaneLoader.setController(new SelectStageController(this));

		this.selectPane = selectPaneLoader.load();


		gamePaneLoader = new FXMLLoader(this.getClass().getResource("view/gameStage.fxml"));
		gamePaneLoader.setController(new GameStageController(this));

		this.gamePane = gamePaneLoader.load();


		creditsPaneLoader = new FXMLLoader(this.getClass().getResource("view/creditsStage.fxml"));
		creditsPaneLoader.setController(new CreditsStageController(this));

		this.creditsPane = creditsPaneLoader.load();


		optionsPaneLoader = new FXMLLoader(this.getClass().getResource("view/optionsStage_v2.fxml"));
		optionsPaneLoader.setController(new OptionsStageMenuController(this));

		this.optionsPane = optionsPaneLoader.load();


		optionsDifficultyPaneLoader = new FXMLLoader(this.getClass().getResource("view/optionsStageDifficulty.fxml"));
		optionsDifficultyPaneLoader.setController(new OptionsStageDifficultyController(this));

		this.optionsDifficultyPane = optionsDifficultyPaneLoader.load();


		optionsCustomPaneLoader = new FXMLLoader(this.getClass().getResource("view/optionsStage_Custom.fxml"));
		optionsCustomPaneLoader.setController(new OptionsStageCustomController(this));

		this.optionsCustomPane = optionsCustomPaneLoader.load();


		multiPaneLoader = new FXMLLoader(this.getClass().getResource("view/multiStage.fxml"));
		multiPaneLoader.setController(new MultiStageController(this));

		this.multiPane = multiPaneLoader.load();


		attentePaneLoader = new FXMLLoader(this.getClass().getResource("view/AttenteStage.fxml"));
		attentePaneLoader.setController(new AttenteStageController(this));

		this.attentePane = attentePaneLoader.load();
	}

	public void changePane(PaneIndex index) {

		this.mainPane.getChildren().clear();

		if (index == PaneIndex.GAME_PANE) {
			gamePaneLoader = new FXMLLoader(this.getClass().getResource("view/gameStage.fxml"));
			gamePaneLoader.setController(new GameStageController(this));

			try {
				this.gamePane = gamePaneLoader.load();
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.mainPane.setCenter(this.gamePane);
		} else if (index == PaneIndex.CREDITS_PANE) {
			this.mainPane.setCenter(this.creditsPane);
		} else if (index == PaneIndex.OPTIONS_MENU_PANE) {

			optionsPaneLoader = new FXMLLoader(this.getClass().getResource("view/optionsStage_v2.fxml"));
			optionsPaneLoader.setController(new OptionsStageMenuController(this));

			try {
				this.optionsPane = optionsPaneLoader.load();
			} catch (IOException e) {
				e.printStackTrace();
			}

			this.mainPane.setCenter(this.optionsPane);
		} else if (index == PaneIndex.START_PANE) {
			this.mainPane.setCenter(this.selectPane);
		} else if (index == PaneIndex.OPTIONS_DIFFICULTY_PANE) {
			this.mainPane.setCenter(this.optionsDifficultyPane);
		}else if (index == PaneIndex.OPTIONS_CUSTOM_PANE) {
			this.mainPane.setCenter(this.optionsCustomPane);
		}else if (index == PaneIndex.MULTI_PANE) {
			this.mainPane.setCenter(this.multiPane);
		}else if (index == PaneIndex.ATTENTE_PANE) {
			this.mainPane.setCenter(this.attentePane);
		}
	}
}