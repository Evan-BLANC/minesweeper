package fr.iut.game.demineur;

import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class MyButtons extends Button {
	private static int nbrFlag=0;
	private static int nbrBomb=0;
	private Boolean bomb;
	private Boolean flag;
	private Boolean discover;
	private int xButton;
	private int yButton;
	private int value;
	
	public MyButtons() {
		this.bomb=false;
		this.flag=false;
		this.discover = false;
		this.value=0;
		nbrFlag=0;
		nbrBomb=0;
	}
	
	public void placeBomb() {
		this.bomb=true;
	}
	
	public void placeFlag() {
		this.flag=true;
	}
	
	public void removeFlag() {
		this.flag=false;
	}
	
	public void removeBomb() {
		this.bomb=false;
	}
	

	public Boolean isBomb() {
		return bomb;
	}
	
	public Boolean isFlag() {
		return flag;
	}
	
	public Boolean isDiscover() {
		return this.discover;
	}
	
	public void discover() {
		this.discover=true;
	}
	
	public void hide() {
		this.discover=false;
	}
	
	public void increment() {
		this.value++;
	}
	
	public int getValue() {
		return this.value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public void clearValue() {
		this.value=0;
	}

	public static void incrementNbrFlags(){
		nbrFlag++;
	}

	public static void decrementNbrFlags(){
		nbrFlag--;
	}

	public static void incrementNbrBomb(){
		nbrBomb++;
	}

	public static int getNbrFlags(){
		return nbrFlag;
	}

	public static int getNbrBomb() {
		return nbrBomb;
	}

	public String toString() {
		return "bomb " + bomb + " - flag " + flag + " - value "+ value;
	}

	public int getxButton() {
		return xButton;
	}

	public void setxButton(int xButton) {
		this.xButton = xButton;
	}

	public int getyButton() {
		return yButton;
	}

	public void setyButton(int yButton) {
		this.yButton = yButton;
	}
}
