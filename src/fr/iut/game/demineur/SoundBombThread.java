package fr.iut.game.demineur;

import javafx.application.Platform;

import java.lang.management.PlatformLoggingMXBean;

public class SoundBombThread extends Thread {
    @Override
    public void run() {
        try {
            Thread.sleep(3926);
            Platform.runLater(() -> Sounds.getSoundBombPlayer().stop());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
