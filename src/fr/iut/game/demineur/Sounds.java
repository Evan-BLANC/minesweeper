package fr.iut.game.demineur;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

public class Sounds {

    private static MediaPlayer musicPlayer = new MediaPlayer(
            new Media(new File("resources/fr/iut/game/demineur/view/sound/musicBackground.wav")
                    .toURI().toString()));

    private static MediaPlayer soundBigButtonPlayer = new MediaPlayer(
            new Media(new File("resources/fr/iut/game/demineur/view/sound/soundBigButton.wav")
                    .toURI().toString()));

    private static MediaPlayer soundLittleButtonPlayer = new MediaPlayer(
            new Media(new File("resources/fr/iut/game/demineur/view/sound/soundLittleButton.wav")
                    .toURI().toString()));

    private static MediaPlayer soundBombPlayer = new MediaPlayer(
            new Media(new File("resources/fr/iut/game/demineur/view/sound/soundExplosion.wav")
                    .toURI().toString()));

    private static MediaPlayer soundRadarPlayer = new MediaPlayer(
            new Media(new File("resources/fr/iut/game/demineur/view/sound/soundRadarPing.wav")
                    .toURI().toString()));

    private static MediaPlayer soundFlagPlayer = new MediaPlayer(
            new Media(new File("resources/fr/iut/game/demineur/view/sound/soundFlagPlanted.wav")
                    .toURI().toString()));

    private static MediaPlayer soundBombDefusedPlayer = new MediaPlayer(
            new Media(new File("resources/fr/iut/game/demineur/view/sound/soundBombDefused.wav")
                    .toURI().toString()));

    private static MediaPlayer soundSpyAlertPlayer = new MediaPlayer(
            new Media(new File("resources/fr/iut/game/demineur/view/sound/soundSpyAlert.wav")
                    .toURI().toString()));

    private static MediaPlayer soundFlashBangPlayer = new MediaPlayer(
            new Media(new File("resources/fr/iut/game/demineur/view/sound/soundFlashBang.wav")
                    .toURI().toString()));

    public static MediaPlayer getMusicPlayer() {
        return musicPlayer;
    }

    public static MediaPlayer getSoundBigButtonPlayer() {
        return soundBigButtonPlayer;
    }
    public static MediaPlayer getSoundLittleButtonPlayer() {
        return soundLittleButtonPlayer;
    }

    public static MediaPlayer getSoundBombPlayer() {
        return soundBombPlayer;
    }
    public static MediaPlayer getSoundRadarPlayer() {
        return soundRadarPlayer;
    }
    public static MediaPlayer getSoundFlagPlayer() {
        return soundFlagPlayer;
    }
    public static MediaPlayer getSoundBombDefusedPlayer() {
        return soundBombDefusedPlayer;
    }
    public static MediaPlayer getSoundSpyAlertPlayer() {
        return soundSpyAlertPlayer;
    }
    public static MediaPlayer getSoundFlashBangPlayer() {
        return soundFlashBangPlayer;
    }

}
