package fr.iut.game.demineur;

import fr.iut.game.demineur.controller.GameStageController;
import javafx.application.Platform;

public class Thread_Radar extends Thread {
    @Override
    public void run() {
        try{
            Thread.sleep(1000);
            Platform.runLater(() ->
                    GameStageController.hideBomb());
        }catch (InterruptedException e){}
    }
}
