package fr.iut.game.demineur;

import fr.iut.game.demineur.controller.GameStageController;
import fr.iut.game.demineur.controller.MultiStageController;
import fr.iut.game.demineur.controller.Reception;
import javafx.application.Platform;

public class Thread_Victoire extends Thread{

    private GameStageController stage;
    private Boolean on = true;
    private static Boolean recu = false;

    public Thread_Victoire(GameStageController stage){this.stage=stage;}
    @Override
    public void run() {
        while(on){
            System.out.println(getRecu());
            if (getRecu()){
                System.out.println("threadVictoire");
                on=false;
                Platform.runLater(() -> {
                    stage.end();
                });
            }
        }
    }

    public void terminate(){
        this.on=false;
    }

    public static void setRecu(Boolean b){
        Thread_Victoire.recu = true;
    }

    public static Boolean getRecu(){
        return Thread_Victoire.recu;
    }
}
