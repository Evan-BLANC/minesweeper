package fr.iut.game.demineur;

import fr.iut.game.demineur.controller.GameStageController;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.text.NumberFormat;
import java.util.Locale;

public class Timer extends Thread {
    GameStageController g;
    TextField temps;
    boolean demarre;
    private int shield = 0;
    private int radar = 0;
    private int spy = 0;
    private int flash = 0;
    private Boolean done=false;

    public void setShield(int shield) {
        this.shield = shield;
    }

    public void setRadar(int radar) {
        this.radar = radar;
    }

    public void setSpy(int spy) {
        this.spy = spy;
    }

    public void setFlash(int flash) {
        this.flash = flash;
    }

    public Timer(GameStageController g, TextField temps){
        this.g=g;
        this.temps=temps;
    }

    public void run(){
        demarre=true;
        Double t= new Double(0.0);
        int compteur=0;

        while (demarre) {
            try{
                this.sleep(100);
                t+=0.1;
                compteur++;
                if(!done) {
                    if (compteur % 10 == 0) {
                        shield++;
                        radar++;
                        flash++;
                        spy++;
                    }

                    if (shield == 20) {
                        Platform.runLater(() -> {
                            g.activeShield();
                        });
                    }
                    if (flash == 20) {
                        Platform.runLater(() -> {
                            g.activeFlash();
                        });
                    }

                    if (radar == 60) {
                        Platform.runLater(() -> {
                            g.activeRadar();
                        });
                    }
                    if (spy == 60) {
                        Platform.runLater(() -> {
                            g.activeSpy();
                        });
                    }
                }
                String n = NumberFormat.getInstance(Locale.ENGLISH).format(t);
                Platform.runLater(() -> {
                    temps.setText(n);
                });
            }
            catch(InterruptedException e){

            }
        }
    }


    public void terminate(){
        demarre=false;
    }
}