/**
 *
 */
package fr.iut.game.demineur.controller;

import fr.iut.game.demineur.PaneIndex;
import fr.iut.game.demineur.Sounds;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.Image;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author evanblanc, avarnier
 *
 */
public class AttenteStageController implements Initializable {


	@FXML
	private Button btReady;



	private PaneSelectorInterface selector;

	private static final Image im = new Image(AttenteStageController.class.getResourceAsStream("../view/Images/imageTitre.jpg"));

	public AttenteStageController(PaneSelectorInterface selector) {
		this.selector = selector;
	}


	/* (non-Javadoc)
	 * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		btReady.setOnAction(evt ->{
			/*Sounds.getSoundBigButtonPlayer().stop();
			Sounds.getSoundBigButtonPlayer().setVolume(0.25);
			Sounds.getSoundBigButtonPlayer().play();
			try {
				Thread.sleep(418);
				Platform.runLater(() -> Sounds.getSoundBigButtonPlayer().stop());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}*/
			if (Client.getR().getMessage()!=null) {
				if (Client.getR().getMessage().equals("READY") ) {
					MultiStageController.getC().getE().envoieMessage("READY");
					try {
						selector.changePane(PaneIndex.GAME_PANE);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

		});
	}


}
