package fr.iut.game.demineur.controller;

import fr.iut.game.demineur.PaneIndex;
import javafx.application.Platform;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client implements Runnable{
    private Socket socket = null;
    private int port = 2000;
    private String ad = "localhost";
    private BufferedReader in;
    private PrintWriter out;
    private Thread t1;
    private Thread t2;
    private final PaneSelectorInterface selector;
    private Envoi e;
    private static Reception r;

    public Client(PaneSelectorInterface selector){
        this.selector = selector;
    }

    public void run(){
        try {
            System.out.println("Demande de connexion");
            socket = new Socket(ad, port);
            System.out.println("Connecté");

            out = new PrintWriter(socket.getOutputStream());
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            t1 = new Thread(e = new Envoi(out));
            t1.start();

            t2 = new Thread(r = new Reception(in));
            t2.start();

            Platform.runLater(() -> {
                try {
                    selector.changePane(PaneIndex.ATTENTE_PANE
                    );
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

        } catch (UnknownHostException e) {
            System.err.println("Impossible de se connecter à l'adresse "+ad);
        } catch (IOException e) {
            System.err.println("Aucun serveur à l'écoute du port "+Integer.toString(port));
        }
    }

    public static Reception getR() {
        return r;
    }

    public Envoi getE() {
        return e;
    }
}