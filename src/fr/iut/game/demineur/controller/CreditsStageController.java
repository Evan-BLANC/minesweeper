package fr.iut.game.demineur.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import fr.iut.game.demineur.PaneIndex;
import fr.iut.game.demineur.Sounds;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

public class CreditsStageController implements Initializable {

	@FXML
	private BorderPane creditsPane;
	
	@FXML
	private Label creditsLabel;
	
	@FXML
	private Button creditsRetour;
	
	private final PaneSelectorInterface selector;
	
	
	public CreditsStageController(PaneSelectorInterface selector) {
		this.selector = selector;
	}
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		creditsRetour.setOnAction(evt -> {
			try {
				/*Sounds.getSoundLittleButtonPlayer().stop();
				Sounds.getSoundLittleButtonPlayer().setVolume(0.6);
				Sounds.getSoundLittleButtonPlayer().play();
				try {
					Thread.sleep(288);
					Platform.runLater(() -> Sounds.getSoundLittleButtonPlayer().stop());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}*/
				selector.changePane(PaneIndex.START_PANE);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});

	}

}
