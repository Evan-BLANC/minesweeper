package fr.iut.game.demineur.controller;

import java.io.PrintWriter;
import java.util.Scanner;

public class Envoi implements Runnable{
    private PrintWriter out;
    private String message ;
    private boolean pause = false;

    public Envoi(PrintWriter out) {
        this.out = out;
    }

    @Override
    public void run() {
        if(!pause) {
            out.println("");
            out.flush();
        }
    }

    public void envoieMessage(String message){
        this.pause=true;
        this.message = message;
        if (!message.isEmpty()) {
            out.println(message);
            out.flush();
        }
        this.pause=false;
    }
}
