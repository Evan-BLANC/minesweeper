package fr.iut.game.demineur.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import fr.iut.game.demineur.*;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.Random;

import static java.lang.System.out;

public class GameStageController implements Initializable, EventHandler<MouseEvent> {

	@FXML
	private Button btExit;

	@FXML
	private Button btQuitter;

	@FXML
	private Button btRestart;

	@FXML
	private Button btBouclier;

	@FXML
	private Button btEspion;

	@FXML
	private Button btBlind;

	@FXML
	private Button btRadar;

	@FXML
	private BorderPane mainPane;

	@FXML
	private Label labelFlag;

	@FXML
	private Label textFlag;

	@FXML
	protected Label textFlagEnnemi;

	@FXML
	private Pane PaneNbrDrapeauEnnemi;

	@FXML
	private Pane paneCenter;

	@FXML
	private Label labelTime;

	@FXML
	private TextField textTemps = new TextField();

	private Timer t;


	private final PaneSelectorInterface selector;
	
	static GridPane grid = new GridPane();

	static MyButtons[][] boutons;

	private Boolean firstClick;

	private static Boolean gagne=false;

	protected static MyButtons bouton;

	private static Boolean solo=true;

	private Boolean bombPlaced=false;

	private Boolean shield=false; 		// le bouclier est actif ?

	private Boolean shieldLoaded=false; // le bouclier est pret ?

	private Boolean radarLoaded=false; // le radar est pret ?

	private Boolean flashLoaded=false; // le bouclier est pret ?

	private Boolean spyLoaded=false; // le radar est pret ?

	private static Boolean client=false;

	private static  Image imBomb = new Image(GameStageController.class.getResourceAsStream(
			"../view/Images/bombeGifPerdu.gif"));
	private static  Image imFlag = new Image(GameStageController.class.getResourceAsStream(
			"../view/Images/flag.gif"));
	private static  Image bouclierDesacitve = new Image(GameStageController.class.getResourceAsStream(
			"../view/Images/BouclierDesactive.png"));
	private static  Image bouclierDispo = new Image(GameStageController.class.getResourceAsStream(
			"../view/Images/BouclierDispo.gif"));
	private static  Image bouclierActive = new Image(GameStageController.class.getResourceAsStream(
			"../view/Images/BouclierActive.gif"));
	private static  Image radarDesactive = new Image(GameStageController.class.getResourceAsStream(
			"../view/Images/RadarDesactive.png"));
	private static  Image radarDispo = new Image(GameStageController.class.getResourceAsStream(
			"../view/Images/RadarDispo.gif"));
	private static  Image espionDesacitve = new Image(GameStageController.class.getResourceAsStream(
			"../view/Images/BoutonEspionOff.png"));
	private static  Image espionDispo = new Image(GameStageController.class.getResourceAsStream(
			"../view/Images/BoutonEspionOn.gif"));
	private static  Image flashDesacitve = new Image(GameStageController.class.getResourceAsStream(
			"../view/Images/BoutonflashOff.png"));
	private static  Image flashDispo = new Image(GameStageController.class.getResourceAsStream(
			"../view/Images/BoutonflashOn.gif"));

	private Thread_Victoire multiThread = new Thread_Victoire(this);

	private SoundBombThread soundBombThread = new SoundBombThread();

	ImageView imScan = new ImageView(radarDesactive);
	ImageView imScanDispo = new ImageView(radarDispo);
	ImageView imShieldDesact = new ImageView(bouclierDesacitve);
	ImageView imShieldAct = new ImageView(bouclierActive);
	ImageView imShieldDispo = new ImageView(bouclierDispo);
	ImageView imSpyDesac = new ImageView(espionDesacitve);
	ImageView imSpyDispo = new ImageView(espionDispo);
	ImageView imFlashDesac = new ImageView(flashDesacitve);
	ImageView imFlashDispo = new ImageView(flashDispo);
	//------------------------------------------------------------------------------------------------------------------

	public static Boolean getClient() {
		return client;
	}

	public static void setClient(Boolean client) {
		GameStageController.client = client;
	}

	public static Boolean getSolo() {
		return solo;
	}

	public static void setSolo(Boolean solo) {
		GameStageController.solo = solo;
	}

	public static void setGagne(Boolean b){
		gagne=b;
	}

	//------------------------------------------------------------------------------------------------------------------

	public void activeShield(){
		shieldLoaded=true;
		btBouclier.setGraphic(imShieldDispo);
		btBouclier.setDisable(false);
	}

	public void activeRadar(){
		radarLoaded=true;
		btRadar.setDisable(false);
		btRadar.setGraphic(imScanDispo);
	}

	public void activeFlash(){
		flashLoaded=true;
		btBlind.setGraphic(imFlashDispo);
		btBlind.setDisable(false);
	}

	public void activeSpy(){
		spyLoaded=true;
		btEspion.setDisable(false);
		btEspion.setGraphic(imSpyDispo);
	}

	//------------------------------------------------------------------------------------------------------------------

	/*
	 * initialize game settings
	 */
	public void initGame(){
		btBouclier.setDisable(false);
		btRadar.setDisable(false);
		btBlind.setDisable(false);
		btEspion.setDisable(false);
		gagne = false;


		imScan.setFitHeight(80);
		imScan.setFitWidth(80);
		imScanDispo.setFitHeight(80);
		imScanDispo.setFitWidth(80);
		imShieldAct.setFitHeight(80);
		imShieldAct.setFitWidth(80);
		imShieldDesact.setFitHeight(80);
		imShieldDesact.setFitWidth(80);
		imShieldDispo.setFitHeight(80);
		imShieldDispo.setFitWidth(80);
		imSpyDispo.setFitHeight(80);
		imSpyDispo.setFitWidth(80);
		imSpyDesac.setFitHeight(80);
		imSpyDesac.setFitWidth(80);
		imFlashDispo.setFitHeight(80);
		imFlashDispo.setFitWidth(80);
		imFlashDesac.setFitHeight(80);
		imFlashDesac.setFitWidth(80);

		btRadar.setGraphic(imScan);
		btBouclier.setGraphic(imShieldDesact);
		btEspion.setGraphic(imSpyDesac);
		btBlind.setGraphic(imFlashDesac);

		this.firstClick=true;
		for (Node node : grid.getChildren()) {
			((MyButtons)node).setGraphic(null);
			((MyButtons)node).setBackground(new Background(new BackgroundFill(Color.web("#A4A4A4"),
					null, null)));
			((MyButtons)node).removeBomb();
			((MyButtons)node).removeFlag();
			((MyButtons)node).setValue(0);
			((MyButtons)node).hide();
			((MyButtons)node).setText("");
		}
	}
	
	
	public GameStageController(PaneSelectorInterface selector) {
		this.selector = selector;
	}


	public static void hideBomb() {
		for (Node node : grid.getChildren()) {
			if (((MyButtons) node).isBomb() && !((MyButtons) node).isDiscover()) {
				((MyButtons) node).setGraphic(null);
				if(((MyButtons) node).isFlag()){
					ImageView imView = new ImageView(imFlag);
					imView.setFitHeight(((MyButtons) node).getHeight() - 10);
					imView.setFitWidth((((MyButtons) node).getWidth()) - 18);
					((MyButtons) node).setGraphic(imView);
				}else {
					((MyButtons) node).setBackground(new Background(new BackgroundFill(Color.web("#A4A4A4"),
							null, null)));
				}
			}
		}

	}

	public static void showBomb() {
		for (Node node : grid.getChildren()) {
			if (((MyButtons) node).isBomb()) {
				ImageView imView = new ImageView(imBomb);
				imView.setFitHeight(((MyButtons) node).getHeight() - 10);
				imView.setFitWidth(((MyButtons) node).getWidth() - 18);
				((MyButtons) node).setGraphic(imView);
			}
		}
	}

	public static void flashLaCarte(){
		//Faire l'image du flash;
	}

	public static void espionPrendUnDrapeau(){
		boolean aVole=false;
		Random r = new Random();
		while (!aVole) {
			/*for (Node node : grid.getChildren()) {
				if (((MyButtons)node).isFlag()){
					int n = r.nextInt(101);
					if (n < 33) {
						((MyButtons) node).removeFlag();
						MyButtons.decrementNbrFlags();
						if (!solo) {
							if (client) {
								MultiStageController.getC().getE().envoieMessage("flag," + MyButtons.getNbrFlags() + "," + MyButtons.getNbrBomb());
							} else {
								MultiStageController.getS().getE().envoieMessage("flag," + MyButtons.getNbrFlags() + "," + MyButtons.getNbrBomb());
							}
						}
						aVole=true;
						break;
					}
				}
			}*/
			MyButtons temp;
			for (int i =0; i<OptionsStageMenuController.getGridHeight(); i++){
				for (int j=0; j<OptionsStageMenuController.getGridHeight(); j++){
					temp = boutons[i][j];
					if (temp.isFlag()){
						int n = r.nextInt(101);
						if (n < 33) {
							temp.removeFlag();
							temp.setGraphic(null);
							MyButtons.decrementNbrFlags();
							if (!solo) {
								if (client) {
									MultiStageController.getC().getE().envoieMessage("flag," + MyButtons.getNbrFlags() + "," + MyButtons.getNbrBomb());
								} else {
									MultiStageController.getS().getE().envoieMessage("flag," + MyButtons.getNbrFlags() + "," + MyButtons.getNbrBomb());
								}
							}
							aVole=true;
							break;
						}
					}
				}
			}
		}
	}
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		grid.setTranslateX(10);
		grid.setTranslateY(-10);
		System.out.println("initialize GameStageController");
		btBouclier.setVisible(false);
		btRadar.setVisible(false);
		btEspion.setVisible(false);
		btBlind.setVisible(false);
		PaneNbrDrapeauEnnemi.setVisible(false);
		btQuitter.setVisible(false);
		if (!getSolo()){
			PaneNbrDrapeauEnnemi.setVisible(true);
			btQuitter.setVisible(true);
			btRestart.setVisible(false);
			btExit.setVisible(false);
		}

		if (OptionsStageMenuController.isBonus()) {
			if (solo){
				btBouclier.setVisible(true);
				btRadar.setVisible(true);
			}else{
				btBouclier.setVisible(true);
				btRadar.setVisible(true);
				btEspion.setVisible(true);
				btBlind.setVisible(true);
			}
		}
		t = new Timer(this,textTemps);
		grid.getChildren().clear();
		initGrid();
		initGame();
		mainPane.setCenter(paneCenter);
		paneCenter.getChildren().add(grid);
		textFlag.setText("/?");
		if (!solo){
			t.start();
			multiThread.start();
		}

		btExit.setOnAction(evt -> {
			multiThread.terminate();
			if(!solo){
				MultiStageController.stop();
			}
			t.terminate();
			Sounds.getSoundBigButtonPlayer().stop();
			grid.getChildren().clear();
			initGrid();
			initGame();
			grid.getChildren().clear();
			if(!solo){
				if (GameStageController.getClient()){
					MultiStageController.getC().getR().terminate();
				}else{
					MultiStageController.getS().getR().terminate();
				}
			}
			try {
				selector.changePane(PaneIndex.START_PANE);
			} catch (IOException e) {
				e.printStackTrace();
			}
			Sounds.getSoundLittleButtonPlayer().stop();
			Sounds.getSoundLittleButtonPlayer().setVolume(0.6);
			Sounds.getSoundLittleButtonPlayer().play();
			try {
				Thread.sleep(288);
				Platform.runLater(() -> Sounds.getSoundLittleButtonPlayer().stop());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});

		btQuitter.setOnAction(evt -> {
			System.exit(0);
		});

		btRestart.setOnAction(evt -> {
			t.terminate();
			Reception.restart();
			grid.getChildren().clear();
			initGrid();
			initGame();
			grid.getChildren().clear();
			if (!solo){
				if (getClient()){
					try {
						selector.changePane(PaneIndex.ATTENTE_PANE);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}else{
					try {
						selector.changePane(PaneIndex.OPTIONS_MENU_PANE);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}else{
				try {
					selector.changePane(PaneIndex.GAME_PANE);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
            Sounds.getSoundLittleButtonPlayer().stop();
			Sounds.getSoundLittleButtonPlayer().setVolume(0.6);
			Sounds.getSoundLittleButtonPlayer().play();
			try {
				Thread.sleep(288);
				Platform.runLater(() -> Sounds.getSoundLittleButtonPlayer().stop());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		});

		btRadar.setOnAction(evt -> {
			if(bombPlaced && radarLoaded) {
				Sounds.getSoundRadarPlayer().stop();
				radarLoaded=false;
				btRadar.setDisable(true);
				t.setRadar(0);
				showBomb();
				Thread_Radar t = new Thread_Radar();
				t.start();
				btRadar.setGraphic(imScan);
				Sounds.getSoundRadarPlayer().stop();
				Sounds.getSoundRadarPlayer().setVolume(0.4);
				Sounds.getSoundRadarPlayer().play();
			}
		});

		btBouclier.setOnAction(evt -> {
			if(shieldLoaded) {
				shield = true;
				shieldLoaded=false;
				btBouclier.setDisable(false);
				btBouclier.setGraphic(imShieldAct);

				Sounds.getSoundLittleButtonPlayer().stop();
				Sounds.getSoundLittleButtonPlayer().setVolume(0.6);
				Sounds.getSoundLittleButtonPlayer().play();
				/*try {
					Thread.sleep(288);
					Platform.runLater(() -> Sounds.getSoundLittleButtonPlayer().stop());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}*/
			}
		});

		btBlind.setOnAction(evt -> {
			if (flashLoaded){
				flashLoaded=false;
				btBlind.setDisable(true);
				t.setFlash(0);
				Thread_Radar t = new Thread_Radar();
				t.start();
				//envoie d'un message FLASH à l'adversaire
				btBlind.setGraphic(imFlashDesac);
				Sounds.getSoundLittleButtonPlayer().stop();
				Sounds.getSoundLittleButtonPlayer().setVolume(0.6);
				Sounds.getSoundLittleButtonPlayer().play();
				/*try {
					Thread.sleep(288);
					Platform.runLater(() -> Sounds.getSoundLittleButtonPlayer().stop());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}*/
				if(!solo){
					if(client){
						MultiStageController.getC().getE().envoieMessage("FLASH");
					} else {
						MultiStageController.getS().getE().envoieMessage("FLASH");
					}
				}
			}
		});

		btEspion.setOnAction(evt -> {
			if (spyLoaded){
				spyLoaded=false;
				btEspion.setDisable(true);
				t.setSpy(0);
				Thread_Radar t = new Thread_Radar();
				t.start();
				//envoie d'un message SPY à l'adversaire
				btEspion.setGraphic(imSpyDesac);
				Sounds.getSoundLittleButtonPlayer().stop();
				Sounds.getSoundLittleButtonPlayer().setVolume(0.6);
				Sounds.getSoundLittleButtonPlayer().play();
				/*try {
					Thread.sleep(288);
					Platform.runLater(() -> Sounds.getSoundLittleButtonPlayer().stop());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}*/
				if(!solo){
					if(client){
						MultiStageController.getC().getE().envoieMessage("ESPION");
					} else {
						MultiStageController.getS().getE().envoieMessage("ESPION");
					}
				}
			}
		});

	}


	public void initGrid() {
		boutons = new MyButtons[OptionsStageMenuController.getGridHeight()][OptionsStageMenuController.getGridWidth()];
		for (int idxColumn = 0; idxColumn< OptionsStageMenuController.getGridWidth(); idxColumn++) {
			for (int idxRow = 0; idxRow < OptionsStageMenuController.getGridHeight(); idxRow++) {
				bouton = new MyButtons();
				bouton.setMinSize(OptionsStageMenuController.getSizeButton(), OptionsStageMenuController.getSizeButton());
				bouton.setBorder(new Border(new BorderStroke(Color.BLACK,
						BorderStrokeStyle.SOLID, null, new BorderWidths(0.1))));
				bouton.addEventHandler(MouseEvent.MOUSE_RELEASED, this);
				bouton.setxButton(idxColumn);
				bouton.setyButton(idxRow);
				grid.add(bouton, idxColumn, idxRow);
				boutons[idxColumn][idxRow] = bouton;
			}
		}
	}


	@Override
	public void handle(MouseEvent event) {
		MyButtons bt = (MyButtons)(event.getSource());
		if(event.getButton() == MouseButton.SECONDARY) {// right click
			Sounds.getSoundFlagPlayer().stop();
			Sounds.getSoundFlagPlayer().setVolume(0.5);
			Sounds.getSoundFlagPlayer().play();
			try {
				Thread.sleep(200);
				Sounds.getSoundRadarPlayer().stop();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(bombPlaced) {
				if (!bt.isDiscover()) {
					if (bt.isFlag()) {
						bt.removeFlag();
						MyButtons.decrementNbrFlags();
						if(!solo){
							if(client){
								MultiStageController.getC().getE().envoieMessage("flag,"+MyButtons.getNbrFlags()+","+MyButtons.getNbrBomb());
							} else {
								MultiStageController.getS().getE().envoieMessage("flag,"+MyButtons.getNbrFlags()+","+MyButtons.getNbrBomb());
							}
						}
						bt.setGraphic(null);
						out.println(bt);
					} else {
						bt.placeFlag();
						MyButtons.incrementNbrFlags();
						if(!solo){
							if(client){
								MultiStageController.getC().getE().envoieMessage("flag,"+MyButtons.getNbrFlags()+","+MyButtons.getNbrBomb());
							} else {
								MultiStageController.getS().getE().envoieMessage("flag,"+MyButtons.getNbrFlags()+","+MyButtons.getNbrBomb());
							}
						}
						ImageView imView = new ImageView(imFlag);
						imView.setFitHeight(bt.getHeight() - 10);
						imView.setFitWidth((bt.getWidth()) - 18);
						bt.setGraphic(imView);
						out.println(bt);
					}
				}
			}else{
				Alert alert = new Alert(Alert.AlertType.INFORMATION);
				alert.setTitle("Information");

				ImageView warning = new ImageView(this.getClass().getResource("../view/Images/warning.png").toString());
				warning.setFitHeight(80);
				warning.setFitWidth(80);

				alert.setGraphic(warning);
				alert.setHeaderText(null);
				alert.setContentText("Pas de drapeau avant d'avoir decouvert la grille !");
				Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
				stage.getIcons().add(imBomb);

				alert.showAndWait();
			}
		}else { // left click DOUBLE
			if (event.getClickCount() == 2) {
				discoverDoubleClique(bt);
			} else {//Clique SIMPLE
				/*Sounds.getSoundLittleButtonPlayer().stop();
				Sounds.getSoundLittleButtonPlayer().setVolume(0.6);
				Sounds.getSoundLittleButtonPlayer().play();
				try {
					Thread.sleep(288);
					Sounds.getSoundLittleButtonPlayer().stop();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}*/
				if (firstClick) {
					bombPlaced=true;
					if(solo){
						t.start();
					}
					bt.discover();
					bt.setBackground(new Background(new BackgroundFill(Color.web("#D8D8D8"), null, null)));
					/*				System.out.println("c'est le premier click");*/
					placeAllBomb();
					afficheNum(bt);
					this.firstClick = false;
					discoverAround(bt);
				} else {
					if (bt.isBomb()) {
						if(shield){
							shield = false;
							btBouclier.setDisable(true);
							t.setShield(0);
							ImageView imView = new ImageView(imBomb);
							imView.setFitHeight(bt.getHeight() - 10);
							imView.setFitWidth(bt.getWidth() - 18);
							bt.setGraphic(null);
							bt.setGraphic(imView);
							bt.discover();
							bt.placeFlag();
							btBouclier.setGraphic(imShieldDesact);
						}else{
							gagne=false;
							end();
						}
					} else {
						discoverAround(bt);
					}
				}
				if (!(bt.isDiscover()) && (bt.isFlag()) && !bt.isBomb()) {
					bt.setGraphic(null);
					afficheNum(bt);
					MyButtons.decrementNbrFlags();
					if(!solo){
						if(client){
							MultiStageController.getC().getE().envoieMessage("flag,"+MyButtons.getNbrFlags()+","+MyButtons.getNbrBomb());
						} else {
							MultiStageController.getS().getE().envoieMessage("flag,"+MyButtons.getNbrFlags()+","+MyButtons.getNbrBomb());
						}
					}
				}
				bt.discover();
				bt.setBackground(new Background(new BackgroundFill(Color.web("#D8D8D8"), null, null)));
			}
		}
		if (bombPlaced && isFinish()){
			end();
		}
		textFlag.setText(MyButtons.getNbrFlags()+"/"+MyButtons.getNbrBomb());
	}


	private Boolean isFinish() {
		for (Node node : grid.getChildren()) {
			if( ( ((MyButtons) node).isBomb() && !((MyButtons) node).isFlag() ) || ( ((MyButtons)node).isFlag() && !((MyButtons)node).isBomb() ) ) {
				return false;
			}
		}
		gagne =true;
		return true;
	}


	/**
	 * @param bt le bouton sur lequel on va afficher sa valeur
	 *           Affiche le nombre de bombe autour du bouton
	 */
	private void afficheNum(MyButtons bt) {
		// affiche le nombre de bombes a proximiter du bouton
		if(!(bt.getValue() == 0)) {
			if(bt.isFlag()) {
				bt.removeFlag();
				bt.setGraphic(null);
			}
			bt.setText(bt.getValue()+"");
		}
	}


	public void end() {
		out.println("fini !");
		t.terminate();
		for (Node node : grid.getChildren()) {
			((MyButtons)node).discover();
			((MyButtons)node).setBackground(new Background(new BackgroundFill(Color.web("#D8D8D8"),
					null, null)));
			if(((MyButtons)node).isBomb()) {
				ImageView imView = new ImageView(imBomb);
				imView.setFitHeight(((MyButtons)node).getHeight()-10);
				imView.setFitWidth(((MyButtons)node).getWidth()-18);
				((MyButtons)node).setGraphic(imView);
			}else {
				afficheNum(((MyButtons)node));
			}
			((MyButtons)node).clearValue();
		}

		if(gagne) {
			if (!solo){
				if (getClient()){
					MultiStageController.getC().getE().envoieMessage("GAGNER");
					System.out.println("Gagner envoyer par le client");
					//pop up gagne
					popUpGagne();
				}else{
					MultiStageController.getS().getE().envoieMessage("GAGNER");
					System.out.println("Gagner envoyer par le serveur");
					//pop up gagne
					popUpGagne();
				}
			} else {
				//pop up gagne
				popUpGagne();
			}
		} else {
			if (!solo) {
				if (getClient()) {
					MultiStageController.getC().getE().envoieMessage("PERDRE");
					System.out.println("Perdre envoyer par le client");
					//pop up perdu
					popUpPerdu();
				} else {
					MultiStageController.getS().getE().envoieMessage("PERDRE");
					System.out.println("perdre envoyer par le serveur");
					//pop up perdu
					popUpPerdu();
				}
			} else {
				//pop up perdu
				popUpPerdu();
			}
		}
	}

	private void popUpPerdu() {
		System.out.println("Perdu");
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle("Aie...");

		ImageView bombegif = new ImageView(this.getClass().getResource("../view/Images/explosion1.png").toString());
		bombegif.setFitHeight(100);
		bombegif.setFitWidth(100);

		alert.setGraphic(bombegif);
		alert.setHeaderText(null);
		alert.setContentText("BOUM !!! Dommage, recommence !");
		Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
		stage.getIcons().add(imBomb);

		Sounds.getSoundBombPlayer().stop();
		Sounds.getSoundBombPlayer().setVolume(0.6);
		Sounds.getSoundBombPlayer().play();

		soundBombThread.start();
		alert.showAndWait();
	}

	private void popUpGagne() {
		System.out.println("gagné");
		Alert alert1 = new Alert(Alert.AlertType.INFORMATION);
		alert1.setTitle("Victoire");

		ImageView bombegif = new ImageView(this.getClass().getResource("../view/Images/imageVictoire.gif").toString());
		bombegif.setFitHeight(100);
		bombegif.setFitWidth(100);

		alert1.setGraphic(bombegif);
		alert1.setHeaderText(null);
		alert1.setContentText("Bien joué, c'est gagné !! \n Vous avez mis " + textTemps.getText() + " secondes");
		Stage stage = (Stage) alert1.getDialogPane().getScene().getWindow();
		stage.getIcons().add(new Image(GameStageController.class.getResourceAsStream(
				"../view/Images/imBomb.jpg")));
		Sounds.getSoundBombDefusedPlayer().stop();
		Sounds.getSoundBombDefusedPlayer().setVolume(0.6);
		Sounds.getSoundBombDefusedPlayer().play();

		soundBombThread.start();
		alert1.showAndWait();
	}


	private void placeAllBomb() {
		Random rand = new Random();
		int n;
		int nb=0;
	    for (Node node : grid.getChildren()) {
	    	if( !((MyButtons)node).isDiscover() && nb < OptionsStageMenuController.getNbrBombes() ) {
	    		n = rand.nextInt(101);
	    		//System.out.println(n);
	    		if(OptionsStageMenuController.getProba() > n) {
	    			//System.out.println((OptionsStageMenuController.getProba())*100);
	    			((MyButtons)node).placeBomb();
					((MyButtons)node).incrementNbrBomb();
	    			nb++;
	    			
	    			//on implement la value des cases autour de la bomb 
	    			int xBomb = ((MyButtons)node).getxButton();
	    			int yBomb = ((MyButtons)node).getyButton();
	    			for (Node nodebis : grid.getChildren()) {
	    				int xRech = ((MyButtons)nodebis).getxButton();
	    				int yRech = ((MyButtons)nodebis).getyButton();
	    				
	    				if(xRech == xBomb-1 || xRech == xBomb || xRech == xBomb+1) {
	    					if(yRech==yBomb-1 || yRech == yBomb || yRech==yBomb+1) {
	    						((MyButtons)nodebis).increment();
	    					}
	    				}
	    			}
	    		}
	    	}
	    }
	}
	
	public void discoverAround(MyButtons btd) { // À modifier !!
		int xdisc = btd.getxButton();
		int ydisc = btd.getyButton();

		if (!btd.isDiscover()) {
			estADecouvir(btd);
		}
//faire decouvrir si ==0
		MyButtons temp;
		if (btd.getValue()==0){
			decouvrir(btd, ydisc, xdisc);
		}
	}



	/**
	 * @param btd on va effectuer un test sur le bouton rentrer en parametre
	 *            On verifie que le bouton ne possede pas de bombe, ni de drapeau. Si c'est le cas alors on le decouvre
	 *            et on affiche sa valeur.
	 */
	private void estADecouvir(MyButtons btd) {
		if (!btd.isFlag() && !btd.isBomb()){
			btd.discover();
			btd.setBackground(new Background(new BackgroundFill(Color.web("#D8D8D8"), null, null)));
			afficheNum(btd);
		}
	}

	public void discoverDoubleClique(MyButtons btd) { // À modifier !!
		int xdisc = btd.getxButton();
		int ydisc = btd.getyButton();
		int n = btd.getValue();
		int nb=0;
		MyButtons temp;
		for (int i=xdisc-1; i<= xdisc+1; i++){
			for (int j=ydisc-1; j<= ydisc+1; j++){
				if (i>=0 && j>=0 && j< boutons.length && i< boutons.length){
					temp = boutons[i][j];
					if (temp.isFlag()){
						if(temp.isBomb()) {
							nb++;
						}else {
							end();
						}
					}
				}
			}
		}
		if (nb==n){
			decouvrir(btd, ydisc,xdisc);
		}
	}

	public void decouvrir(MyButtons btd, int ydisc, int xdisc){
		MyButtons temp;
		if (ydisc + 1 < boutons.length) {
			temp = boutons[xdisc][ydisc + 1];
			if (!temp.isDiscover()) {
				estADecouvir(temp);
				if (temp.getValue() == 0) {
					discoverAround(temp);
				}
			}
		}
		if (xdisc + 1 < boutons.length) {
			temp = boutons[xdisc + 1][ydisc];
			if (!temp.isDiscover()) {
				estADecouvir(temp);
				if (temp.getValue() == 0) {
					discoverAround(temp);
				}
			}
		}
		if (ydisc - 1 >= 0) {
			temp = boutons[xdisc][ydisc - 1];
			if (!temp.isDiscover()) {
				estADecouvir(temp);
				if (temp.getValue() == 0) {
					discoverAround(temp);
				}
			}
		}
		if (xdisc - 1 >= 0) {
			temp = boutons[xdisc - 1][ydisc];
			if (!temp.isDiscover()) {
				estADecouvir(temp);
				if (temp.getValue() == 0) {
					discoverAround(temp);
				}
			}
		}
		if (xdisc-1>=0 && ydisc+1< boutons.length){
			temp = boutons[xdisc-1][ydisc+1];
			if (!temp.isDiscover()){
				estADecouvir(temp);
				if (temp.getValue() == 0) {
					discoverAround(temp);
				}
			}
		}
		if (xdisc+1<boutons.length && ydisc+1< boutons.length){
			temp = boutons[xdisc+1][ydisc+1];
			if (!temp.isDiscover()){
				estADecouvir(temp);
				if (temp.getValue() == 0) {
					discoverAround(temp);
				}
			}
		}
		if (xdisc-1>=0 && ydisc-1>=0){
			temp = boutons[xdisc-1][ydisc-1];
			if (!temp.isDiscover()){
				estADecouvir(temp);
				if (temp.getValue() == 0) {
					discoverAround(temp);
				}
			}
		}
		if (xdisc+1<boutons.length && ydisc-1>=0){
			temp = boutons[xdisc+1][ydisc-1];
			if (!temp.isDiscover()){
				estADecouvir(temp);
				if (temp.getValue() == 0) {
					discoverAround(temp);
				}
			}
		}
	}

}