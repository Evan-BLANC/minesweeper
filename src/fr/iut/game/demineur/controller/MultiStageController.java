package fr.iut.game.demineur.controller;

import fr.iut.game.demineur.DemineurLauncher;
import fr.iut.game.demineur.PaneIndex;
import fr.iut.game.demineur.Sounds;
import fr.iut.game.demineur.Thread_Victoire;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.URL;
import java.util.ResourceBundle;

public class MultiStageController implements Initializable {

    @FXML
    private BorderPane optionPane;

    @FXML
    private Button btHote;

    @FXML
    private Button btInvite;

    @FXML
    private Button btReturn;


    private final PaneSelectorInterface selector;
    private static Thread t;
    private static Serveur s;
    private static Client c;

    public MultiStageController(PaneSelectorInterface selector) {
        this.selector = selector;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        btHote.setOnAction(evt -> {
            /*Sounds.getSoundBigButtonPlayer().stop();
            Sounds.getSoundBigButtonPlayer().setVolume(0.25);
            Sounds.getSoundBigButtonPlayer().play();
            try {
                Thread.sleep(418);
                Platform.runLater(() -> Sounds.getSoundBigButtonPlayer().stop());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/

            GameStageController.setClient(false);
            t = new Thread(s = new Serveur(selector));
            t.start();
        });

        btInvite.setOnAction(evt-> {
            /*Sounds.getSoundBigButtonPlayer().stop();
            Sounds.getSoundBigButtonPlayer().setVolume(0.25);
            Sounds.getSoundBigButtonPlayer().play();
            try {
                Thread.sleep(418);
                Platform.runLater(() -> Sounds.getSoundBigButtonPlayer().stop());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            GameStageController.setClient(true);
            t = new Thread(c = new Client(selector));
            t.start();
        });

        btReturn.setOnAction(evt -> {
            /*Sounds.getSoundBigButtonPlayer().stop();
            Sounds.getSoundBigButtonPlayer().setVolume(0.25);
            Sounds.getSoundBigButtonPlayer().play();
            try {
                Thread.sleep(418);
                Platform.runLater(() -> Sounds.getSoundBigButtonPlayer().stop());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            try {
                GameStageController.setSolo(true);
                selector.changePane(PaneIndex.START_PANE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public static Serveur getS() {
        return s;
    }

    public static Client getC() {
        return c;
    }

    public static void stop(){
        t.interrupt();
        Thread_Victoire.setRecu(false);
    }
}
