package fr.iut.game.demineur.controller;

import fr.iut.game.demineur.PaneIndex;
import fr.iut.game.demineur.Sounds;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class OptionsStageCustomController implements Initializable {

    @FXML
    private BorderPane optionPane;

    @FXML
    private TextField tfHauteur;

    @FXML
    private TextField tfProba;

    @FXML
    private TextField tfNbrBomb;

    @FXML
    private Button btSubmit;

    @FXML
    private Button btReturn;

    @FXML
    private ButtonBar buttonBar;

    private final PaneSelectorInterface selector;

    public OptionsStageCustomController(PaneSelectorInterface selector) {
        this.selector = selector;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        btSubmit.setOnAction(evt -> {
            /*Sounds.getSoundLittleButtonPlayer().stop();
            Sounds.getSoundLittleButtonPlayer().setVolume(0.6);
            Sounds.getSoundLittleButtonPlayer().play();
            try {
                Thread.sleep(288);
                Platform.runLater(() -> Sounds.getSoundLittleButtonPlayer().stop());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            if (Integer.parseInt(tfHauteur.getText()) <= 0 || Integer.parseInt(tfNbrBomb.getText()) <= 0 ||Integer.parseInt(tfProba.getText()) <= 0){
                System.out.println("Erreur d'entreés !");
                try {
                    selector.changePane(PaneIndex.OPTIONS_CUSTOM_PANE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else {
                OptionsStageMenuController.setGridHeight(Integer.parseInt(tfHauteur.getText()));
                OptionsStageMenuController.setGridWidth(Integer.parseInt(tfHauteur.getText()));
                OptionsStageMenuController.setProba(Integer.parseInt(tfProba.getText()));
                OptionsStageMenuController.setNbrBombes(Integer.parseInt(tfNbrBomb.getText()));
                OptionsStageMenuController.setSizeButton(490/Integer.parseInt(tfHauteur.getText()));
                changePaneSoloMulti();
            }
        });
        btReturn.setOnAction(evt -> {
            try {
                selector.changePane(PaneIndex.OPTIONS_MENU_PANE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
    private void changePaneSoloMulti() {
        if (GameStageController.getSolo()){
            try {
                /*Sounds.getSoundLittleButtonPlayer().stop();
                Sounds.getSoundLittleButtonPlayer().setVolume(0.6);
                Sounds.getSoundLittleButtonPlayer().play();
                try {
                    Thread.sleep(288);
                    Platform.runLater(() -> Sounds.getSoundLittleButtonPlayer().stop());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
                selector.changePane(PaneIndex.START_PANE);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            try {
                /*Sounds.getSoundLittleButtonPlayer().stop();
                Sounds.getSoundLittleButtonPlayer().setVolume(0.6);
                Sounds.getSoundLittleButtonPlayer().play();
                try {
                    Thread.sleep(288);
                    Platform.runLater(() -> Sounds.getSoundLittleButtonPlayer().stop());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
                selector.changePane(PaneIndex.OPTIONS_MENU_PANE);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
