package fr.iut.game.demineur.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import fr.iut.game.demineur.PaneIndex;
import fr.iut.game.demineur.Sounds;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.Label;
import javafx.scene.layout.*;

public class OptionsStageDifficultyController implements Initializable {

    @FXML
    private BorderPane optionPane;

    @FXML
    private Label labelOption;

    @FXML
    private Button btRetour;


    @FXML
    private Button btDifficile;

    @FXML
    private Button btMoyen;

    @FXML
    private Button btFacile;

    @FXML
    private ButtonBar buttonBar;

    private final PaneSelectorInterface selector;

    public OptionsStageDifficultyController(PaneSelectorInterface selector) {
        this.selector = selector;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btFacile.setOnAction(evt -> {
           /* Sounds.getSoundLittleButtonPlayer().stop();
            Sounds.getSoundLittleButtonPlayer().setVolume(0.6);
            Sounds.getSoundLittleButtonPlayer().play();
            try {
                Thread.sleep(288);
                Platform.runLater(() -> Sounds.getSoundLittleButtonPlayer().stop());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            OptionsStageMenuController.setGridHeight(8);
            OptionsStageMenuController.setGridWidth(8);
            OptionsStageMenuController.setProba(10);
            OptionsStageMenuController.setSizeButton(59);

            try {
                selector.changePane(PaneIndex.OPTIONS_MENU_PANE);

            } catch (IOException e) {
                e.printStackTrace();
            }

        });

        btMoyen.setOnAction(evt ->{
            /*Sounds.getSoundLittleButtonPlayer().stop();
            Sounds.getSoundLittleButtonPlayer().setVolume(0.6);
            Sounds.getSoundLittleButtonPlayer().play();
            try {
                Thread.sleep(288);
                Platform.runLater(() -> Sounds.getSoundLittleButtonPlayer().stop());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            OptionsStageMenuController.setGridHeight(12);
            OptionsStageMenuController.setGridWidth(12);
            OptionsStageMenuController.setProba(12);
            OptionsStageMenuController.setSizeButton(39);

            try {
                selector.changePane(PaneIndex.OPTIONS_MENU_PANE);

            } catch (IOException e) {
                e.printStackTrace();
            }

        });

        btDifficile.setOnAction(evt ->{
            /*Sounds.getSoundLittleButtonPlayer().stop();
            Sounds.getSoundLittleButtonPlayer().setVolume(0.6);
            Sounds.getSoundLittleButtonPlayer().play();
            try {
                Thread.sleep(288);
                Platform.runLater(() -> Sounds.getSoundLittleButtonPlayer().stop());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            OptionsStageMenuController.setGridHeight(15);
            OptionsStageMenuController.setGridWidth(15);
            OptionsStageMenuController.setProba(12);
            OptionsStageMenuController.setSizeButton(31);

            try {
                selector.changePane(PaneIndex.OPTIONS_MENU_PANE);

            } catch (IOException e) {
                e.printStackTrace();
            }

        });

        btRetour.setOnAction(evt ->{
            /*Sounds.getSoundLittleButtonPlayer().stop();
            Sounds.getSoundLittleButtonPlayer().setVolume(0.6);
            Sounds.getSoundLittleButtonPlayer().play();
            try {
                Thread.sleep(288);
                Platform.runLater(() -> Sounds.getSoundLittleButtonPlayer().stop());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            try {
                selector.changePane(PaneIndex.OPTIONS_MENU_PANE);

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
