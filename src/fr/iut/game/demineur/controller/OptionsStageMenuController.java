package fr.iut.game.demineur.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import fr.iut.game.demineur.PaneIndex;
import fr.iut.game.demineur.Sounds;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

import javax.sound.midi.MidiChannel;

public class OptionsStageMenuController implements Initializable {
	private static int gridWidth=12;
	private static int gridHeight=12;
	private static int sizeButton=39;
	private static int proba =12;
	private static int nbrBombes=10000000;
	private static boolean bonus=false;
	private static boolean musique=true;

	public static int getNbrBombes() {
		return nbrBombes;
	}
	public static void setNbrBombes(int bombes){
		nbrBombes = bombes;
	}

	public static void setGridWidth(int width) {
		gridWidth=width;
	}
	public static void setGridHeight(int height) {
		gridHeight=height;
	}

	public static void setProba(int Bomb) {
		proba =Bomb;
	}

	public static int getSizeButton() {
		return sizeButton;
	}

	public static void setSizeButton(int sizeButton) {
		OptionsStageMenuController.sizeButton = sizeButton;
	}

	public static int getGridWidth() {
		return gridWidth;
	}

	public static int getGridHeight() {
		return gridHeight;
	}

	public static int getProba() {
		return proba;
	}

	public static boolean isBonus() {
		return bonus;
	}

	public static void setBonus(boolean bonus) {
		OptionsStageMenuController.bonus = bonus;
	}

	@FXML
	private BorderPane optionPane;
	
	@FXML
	private Label labelOption;

	@FXML
	private Button btDifficulty;
	
	@FXML
	private Button btCustom;

	@FXML
	private Button btReturn;

	@FXML
	private Button btValider;

	@FXML
	private ButtonBar buttonBar;

	@FXML
	private CheckBox checkOption;

	@FXML
	private CheckBox checkMusic;
	
	private final PaneSelectorInterface selector;
	
	public OptionsStageMenuController(PaneSelectorInterface selector) {
		this.selector = selector;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		if(musique) {
			checkMusic.setSelected(true);
		}else{
			checkMusic.setSelected(false);
		}
		if(bonus) {
			checkOption.setSelected(true);
		}else{
			checkOption.setSelected(false);
		}
		System.out.println(GameStageController.getSolo());
		if (GameStageController.getSolo()) {
			btValider.setVisible(false);
			btReturn.setVisible(true);
		} else {
			btValider.setVisible(true);
			btReturn.setVisible(false);
		}

		btReturn.setOnAction(evt -> {
			/*Sounds.getSoundLittleButtonPlayer().stop();
			Sounds.getSoundLittleButtonPlayer().setVolume(0.6);
			Sounds.getSoundLittleButtonPlayer().play();
			try {
				Thread.sleep(288);
				Platform.runLater(() -> Sounds.getSoundLittleButtonPlayer().stop());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}*/
			Sounds.getSoundLittleButtonPlayer().stop();
			if (checkOption.isSelected()){
				setBonus(true);
			}else{
				setBonus(false);
			}
			try {
				selector.changePane(PaneIndex.START_PANE);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		
		btDifficulty.setOnAction(evt ->{
			/*Sounds.getSoundLittleButtonPlayer().stop();
			Sounds.getSoundLittleButtonPlayer().setVolume(0.6);
			Sounds.getSoundLittleButtonPlayer().play();
			try {
				Thread.sleep(288);
				Platform.runLater(() -> Sounds.getSoundLittleButtonPlayer().stop());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}*/
			if (checkOption.isSelected()){
				setBonus(true);
			}else{
				setBonus(false);
			}
			try {
				selector.changePane(PaneIndex.OPTIONS_DIFFICULTY_PANE);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		
		btCustom.setOnAction(evt ->{
			/*Sounds.getSoundLittleButtonPlayer().stop();
			Sounds.getSoundLittleButtonPlayer().setVolume(0.6);
			Sounds.getSoundLittleButtonPlayer().play();
			try {
				Thread.sleep(288);
				Platform.runLater(() -> Sounds.getSoundLittleButtonPlayer().stop());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}*/
			if (checkOption.isSelected()){
				setBonus(true);
			}else{
				setBonus(false);
			}
			try {
				selector.changePane(PaneIndex.OPTIONS_CUSTOM_PANE);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});

		checkMusic.setOnAction(evt -> {
			if (musique) {
				musique=false;
				checkMusic.setSelected(false);
				Sounds.getMusicPlayer().stop();
				Sounds.getMusicPlayer().setAutoPlay(false);
			}
			else {
				musique=true;
				checkMusic.setSelected(true);
				Sounds.getMusicPlayer().setVolume(0.25);
				Sounds.getMusicPlayer().play();
				Sounds.getMusicPlayer().setAutoPlay(true);
			}
		});

		checkOption.setOnAction(evt -> {
			if (bonus) {
				bonus=false;
				checkOption.setSelected(false);
			}
			else {
				bonus=true;
				checkOption.setSelected(true);
			}
		});

		btValider.setOnAction(evt -> {
			if (checkOption.isSelected()){
				setBonus(true);
			}else{
				setBonus(false);
			}
			/*Sounds.getSoundLittleButtonPlayer().stop();
			Sounds.getSoundLittleButtonPlayer().setVolume(0.6);
			Sounds.getSoundLittleButtonPlayer().play();
			try {
				Thread.sleep(288);
				Platform.runLater(() -> Sounds.getSoundLittleButtonPlayer().stop());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}*/
			String s1 = String.valueOf(OptionsStageMenuController.getGridHeight());
			String s2 = String.valueOf(OptionsStageMenuController.getProba());
			String s3 = String.valueOf(OptionsStageMenuController.getNbrBombes());
			String s4 = String.valueOf(OptionsStageMenuController.getSizeButton());
			String s5 = String.valueOf(OptionsStageMenuController.isBonus());
			String message = "init,"+s1+","+s2+","+s3+","+s4+","+s5;

			MultiStageController.getS().getE().envoieMessage(message);
			System.out.println(message);
			while (MultiStageController.getS().getR().getMessage() == null || !MultiStageController.getS().getR().getMessage().equals("INIT_RECU")){
				System.out.println("Attente de reponse INIT");// Le "tant que" ne fonctionne pas s'il est vide
			}

			MultiStageController.getS().getE().envoieMessage("READY");
			while (MultiStageController.getS().getR().getMessage() == null || !MultiStageController.getS().getR().getMessage().equals("READY")){
				System.out.println("Attente de reponse READY");// Le "tant que" ne fonctionne pas s'il est vide
			}

			try {
				selector.changePane(PaneIndex.GAME_PANE);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}
}
