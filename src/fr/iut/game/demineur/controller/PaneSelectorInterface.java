package fr.iut.game.demineur.controller;

import fr.iut.game.demineur.PaneIndex;

import java.io.IOException;

public interface PaneSelectorInterface {

	public void changePane(PaneIndex index) throws IOException;
}
