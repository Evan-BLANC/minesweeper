package fr.iut.game.demineur.controller;

import fr.iut.game.demineur.Thread_Victoire;
import javafx.application.Platform;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Reception implements Runnable{
    private String message="";
    private BufferedReader in;
    private Pattern pattern1 = Pattern.compile("[A-Z]");
    private Pattern pattern2 = Pattern.compile("[a-z0-9.,(true)(false)]");
    private Matcher matcher1;
    private Matcher matcher2;
    private String[] tabMessage;
    private static Boolean on = true;

    public Reception(BufferedReader in){
        this.in = in;
    }

    @Override
    public void run() {
        while(on) {
            try {
                message=in.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            matcher1 = pattern1.matcher(message);
            matcher2 = pattern2.matcher(message);

            if(matcher1.find()) {
                if (message.equals("GAGNER")) {
                    if (GameStageController.getClient()) {
                        MultiStageController.getC().getE().envoieMessage("PERDU");
                        GameStageController.setGagne(false);
                        System.out.println("perdu");
                        recu();
                    } else {
                        MultiStageController.getS().getE().envoieMessage("PERDU");
                        GameStageController.setGagne(false);
                        System.out.println("perdu");
                        recu();
                    }
                } else if (message.equals("PERDRE")) {
                    if (GameStageController.getClient()) {
                        MultiStageController.getC().getE().envoieMessage("GAGNE");
                        GameStageController.setGagne(true);
                        System.out.println("gagne");
                        recu();
                    } else {
                        MultiStageController.getS().getE().envoieMessage("GAGNE");
                        GameStageController.setGagne(true);
                        System.out.println("gagne");
                        recu();
                    }
                }else if (message.equals("ESPION")){
                    if (GameStageController.getClient()) {
                        MultiStageController.getC().getE().envoieMessage("Espion recu");
                        GameStageController.espionPrendUnDrapeau();
                    } else {
                        MultiStageController.getS().getE().envoieMessage("Espion recu");
                        GameStageController.espionPrendUnDrapeau();
                    }
                } else if (message.equals("FLASH")){
                    GameStageController.flashLaCarte();
                }
            } else if(matcher2.find()){
                tabMessage=message.split(",");
                if (tabMessage[0].equals("init")) {
                    for (int i=0; i<tabMessage.length; i++){
                        System.out.println(tabMessage[i]);
                    }
                    OptionsStageMenuController.setGridHeight(Integer.parseInt(tabMessage[1]));
                    OptionsStageMenuController.setGridWidth(Integer.parseInt(tabMessage[1]));
                    OptionsStageMenuController.setProba(Integer.parseInt(tabMessage[2]));
                    OptionsStageMenuController.setNbrBombes(Integer.parseInt(tabMessage[3]));
                    OptionsStageMenuController.setSizeButton(Integer.parseInt(tabMessage[4]));
                    OptionsStageMenuController.setBonus(Boolean.parseBoolean(tabMessage[5]));
                    MultiStageController.getC().getE().envoieMessage("INIT_RECU");
                } else if (tabMessage[0].equals("flag")){
                    System.out.println(tabMessage[1]+"/"+tabMessage[2]);
                }
            }
        }
    }

    private void recu() {
        Platform.runLater(() -> {
            Thread_Victoire.setRecu(true);
            System.out.println(Thread_Victoire.getRecu());
            terminate();
        });
    }

    public static void terminate(){Reception.on=false;}

    public static void restart(){Reception.on=true;}

    public String getMessage() {
        return message;
    }


}
