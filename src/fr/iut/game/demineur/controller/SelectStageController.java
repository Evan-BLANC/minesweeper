/**
 * 
 */
package fr.iut.game.demineur.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import fr.iut.game.demineur.DemineurLauncher;
import fr.iut.game.demineur.PaneIndex;
import fr.iut.game.demineur.Sounds;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.MediaPlayer;

/**
 * @author evanblanc, avarnier
 *
 */
public class SelectStageController implements Initializable {
	
	
	@FXML
	private Button btStart;
	
	@FXML
	private Button btOptions;
	
	@FXML
	private Button btCredits;

	@FXML
	private Button btQuitter;

	@FXML
	private Button btMulti;
	
	@FXML
	private ImageView ivGame;
	
	
	private PaneSelectorInterface selector;
	
	private static final Image im = new Image(SelectStageController.class.getResourceAsStream("../view/Images/imageTitre.jpg"));

	public SelectStageController(PaneSelectorInterface selector) {
		this.selector = selector;
	}
	
	
	/* (non-Javadoc)
	 * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		Sounds.getMusicPlayer().setVolume(0.25);
		Sounds.getMusicPlayer().play();
		Sounds.getMusicPlayer().setAutoPlay(true);
		Sounds.getMusicPlayer().setCycleCount(MediaPlayer.INDEFINITE);

		btStart.setOnAction(evt -> {
			/*Sounds.getSoundBigButtonPlayer().setVolume(0.25);
			Sounds.getSoundBigButtonPlayer().play();*/
			try {
				this.selector.changePane(PaneIndex.GAME_PANE);
			} catch (IOException e) {
				e.printStackTrace();

			}
		});
		
		btCredits.setOnAction(evt -> {
			/*Sounds.getSoundBigButtonPlayer().setVolume(0.25);
			Sounds.getSoundBigButtonPlayer().play();*/
			try {
				this.selector.changePane(PaneIndex.CREDITS_PANE);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		
		btOptions.setOnAction(evt -> {
			/*Sounds.getSoundBigButtonPlayer().setVolume(0.25);
			Sounds.getSoundBigButtonPlayer().play();*/
			try {
				this.selector.changePane(PaneIndex.OPTIONS_MENU_PANE);
				/*try {
					Thread.sleep(418);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Platform.runLater(() -> Sounds.getSoundBigButtonPlayer().stop());*/
			} catch (IOException e) {
				e.printStackTrace();
			}
		});

		btMulti.setOnAction(evt -> {
			/*Sounds.getSoundBigButtonPlayer().setVolume(0.25);
			Sounds.getSoundBigButtonPlayer().play();*/
			try {
				GameStageController.setSolo(false);
				this.selector.changePane(PaneIndex.MULTI_PANE);
				/*try {
					Thread.sleep(418);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Platform.runLater(() -> Sounds.getSoundBigButtonPlayer().stop());*/
			} catch (IOException e) {
				e.printStackTrace();
			}
		});

		btQuitter.setOnAction(evt -> {
			/*Sounds.getSoundBigButtonPlayer().setVolume(0.25);
			Sounds.getSoundBigButtonPlayer().play();
			try {
				Thread.sleep(418);
				Platform.runLater(() -> Sounds.getSoundBigButtonPlayer().stop());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}*/
			System.exit(0);
		});

	}
	
}
