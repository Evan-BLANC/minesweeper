package fr.iut.game.demineur.controller;

import fr.iut.game.demineur.PaneIndex;
import javafx.application.Platform;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class Serveur implements Runnable{
    private ServerSocket serveurSocket = null;
    private int port = 2000;
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private Thread t1;
    private Thread t2;
    private PaneSelectorInterface selector;
    private static boolean ready=false;
    private Envoi e;
    private Reception r;


    public Serveur(PaneSelectorInterface selector){
        this.selector = selector;
    }

    public void run(){
        try {
            serveurSocket = new ServerSocket(port);
            System.out.println("Le serveur est à l'écoute du por25.46.177.114t "+serveurSocket.getLocalPort());

            socket = serveurSocket.accept();
            System.out.println("Acces autorisé pour "+socket.toString());



            if (!socket.isClosed()) {
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream());

                t1 = new Thread(e = new Envoi(out));
                t1.start();

                t2 = new Thread(r = new Reception(in));
                t2.start();

                Platform.runLater(() -> {
                    try {
                        System.out.println(GameStageController.getSolo());
                        selector.changePane(PaneIndex.OPTIONS_MENU_PANE
                        );
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

            }else{
                try {
                    serveurSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    serveurSocket = null;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Envoi getE() {
        return e;
    }

    public void setE(Envoi e) {
        this.e = e;
    }

    public Reception getR() {
        return r;
    }

    public void setR(Reception r) {
        this.r = r;
    }

    public static boolean isReady() {
        return ready;
    }
}
